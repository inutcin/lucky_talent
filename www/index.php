<?php
require("vendor/autoload.php");

use inutcin\luckytalent as luckytalent;

define("WORLD_WIDTH",200.0);
define("WORLD_HEIGHT",200.0);

define("PERSONS_COUNT", 500 );
define("POSITIVE_EVENTS_COUNT", 500);
define("NEGATIVE_EVENTS_COUNT", 500);

session_start();

if(!isset($_SESSION["world"]) || (isset($_GET["new"]) && $_GET["new"])){
    $world = new luckytalent\World(WORLD_WIDTH, WORLD_HEIGHT);
    $world->personsCount = PERSONS_COUNT;
    $world->positiveEventsCount = POSITIVE_EVENTS_COUNT;
    $world->negativeEventsCount = NEGATIVE_EVENTS_COUNT;
    $world->create();
    $_SESSION["world"] = serialize($world);
}
else{
    $world = unserialize($_SESSION["world"]);
    $world->simulationStep();
    $_SESSION["world"] = serialize($world);
}


?>
<img src="data:image/png;base64,<?= base64_encode($world->exportPng(4))?>">
<?
echo "Шаг ".$world->getSimulationStep();
echo "<pre>";
$topCapital = $world->getCapitalTop(20);
foreach($topCapital as $num=>$item){
    echo ($num+1)."\t";
    echo $item->getCapital()."\t";
    echo intval($item->getTalent()*100)."\t";
    echo $item->getVolition()."\t";
    echo $item->getLucks()."\t";
    echo $item->getUnlucks()."\t";
    echo $item->getSuccesses()."\t";
    echo $item->getFaileds()."\t";
    echo "\n";
}
echo "</pre>";
echo "<hr>";
echo "<pre>";
$topCapital = $world->getCapitalTop(-20);
foreach($topCapital as $num=>$item){
    echo ($num+1)."\t";
    echo $item->getCapital()."\t";
    echo intval($item->getTalent()*100)."\t";
    echo $item->getVolition()."\t";
    echo $item->getLucks()."\t";
    echo $item->getUnlucks()."\t";
    echo $item->getSuccesses()."\t";
    echo $item->getFaileds()."\t";
    echo "\n";
}
echo "</pre>";

die;


