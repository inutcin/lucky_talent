<?php
namespace inutcin\ageometry\numeric;

class Vector
{

    // Координаты вектора
    private $x = 0;
    private $y = 0;
    // Направление вектора (угол с осью Х)
    private $angle = 0;

    function __construct(
        float $x0,
        float $y0,
        float $x1,
        float $y1
    )
    {
        $this->x = $x1-$x0;
        $this->y = $y1-$y0;
        $this->angle = self::getAngleByCoords($this->x, $this->y);
    }

    /**
        Сложение вектора с указанным
        @param $a - вектор с которым надо сложить
        @param $multiplier - множитель (-1), если нужно итоговый вектор
        направить в противоположную сторону
    */
    function add(Vector $a, float $multiplier=1)
    {
       $this->x += $a->getX();
       $this->y += $a->getY();

       $this->x*=$multiplier;
       $this->y*=$multiplier;

       $this->angle = self::getAngleByCoords($this->x, $this->y);
    }

    public function getX():float
    {
        return $this->x;
    }

    public function getY():float
    {
        return $this->y;
    }

    public function getAngle():float
    {
        return $this->angle;
    }

    /**
        Вычисление угла вектора по координатам
        
        @return Угол в радианах
    */
    static function getAngleByCoords(float $dX, float $dY):float
    {
        if($dX==0)return 0;
        
        $result = 0;
        if($dY>0 && $dX>0)
            $result = atan(abs($dY)/abs($dX));
        elseif($dY<0 && $dX>0)
            $result = -atan(abs($dY)/abs($dX));
        elseif($dY>0 && $dX<0)
            $result = pi()-atan(abs($dY)/abs($dX));
        elseif($dY<0 && $dX<0)
            $result = pi()+atan(abs($dY)/abs($dX));
        return $result;
    }
}
