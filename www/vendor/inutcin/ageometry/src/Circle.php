<?php
namespace inutcin\ageometry;

class Circle extends Shape
{
    protected $radius = null;
    protected $area = null;

    function __construct(float $x=0, float $y=0, float $radius=0){
        $this->radius = $radius;
    }

    public function setRadiusRange(float $minRadius=1, float $maxRadius=1)
    {
        $this->radius = $minRadius+($maxRadius-$minRadius)*(rand()/getrandmax());
        $this->width = $this->radius*2;
        $this->height = $this->radius*2;
    }

    public function getRadius()
    {
        return $this->radius;
    }


}
