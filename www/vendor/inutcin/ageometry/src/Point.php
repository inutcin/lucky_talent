<?php
namespace inutcin\ageometry;

class Point{
    private $x = null;
    private $y = null;

    function __construct(float $x, float $y){
        $this->x = $x;
        $this->y = $y;
    }

    function move(float $dX, float $dY)
    {
        $this->x+=$dX;
        $this->y+=$dY;
    }

    function __get($name)
    {
        switch($name){
            case 'x':
                return floatval($this->x);
            break;
            case 'y':
                return floatval($this->y);
            break;
        }
    }

}
