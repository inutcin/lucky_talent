<?php
namespace inutcin\ageometry;

class Shape
{
    protected $center;
    protected $width = null;
    protected $height = null;


    function __construct(Point $center)
    {
        $this->center = $center;
    }

    public function setRandomCenter(){
        $this->center = $this->area->getRandomPoint();
    }

    public function getCenter(){
        return $this->center;
    }

    protected function getRandomPoint():Point
    {
        return new Point(
            $this->center->x 
            - ($this->width/2)
            + ($this->width*rand()/getrandmax()),
            $this->center->y 
            - ($this->height/2)
            + ($this->height*rand()/getrandmax())
        );       
    }

    public function getWidth(){
        return $this->width;
    }

    public function getHeight(){
        return $this->height;
    }
}
