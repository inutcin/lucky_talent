<?php
namespace inutcin\ageometry;

class Area extends Shape
{
    function __construct(Point $center, float $width, float $height)
    {
        parent::__construct($center);
        $this->width = $width;
        $this->height = $height;
    }


}
