<?php
namespace inutcin\luckytalent;

use inutcin\ageometry as ageometry;

class Event extends Agent
{
    
    private $profit = 0;

    function __construct(object $area, int $id=0)
    {
        parent::__construct($area, $id);
    }

    public function setProfitRange(float $minProfit,float $maxProfit)
    {
        $this->profit = $minProfit+($maxProfit-$minProfit)*(rand()/getrandmax());
    }

    public function getProfit()
    {
        return $this->profit;
    }

}
