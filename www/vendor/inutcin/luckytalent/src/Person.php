<?php
namespace inutcin\luckytalent;

use inutcin\ageometry as ageometry;
use inutcin\ageometry\numeric as numeric;

class Person extends Agent 
{
    const MAX_SPEED = 5;

    const STRATEGY_PREDATOR = 0x02;
    const STRATEGY_VICTIM = 0x01;

    private $talent = 0;
    private $volition = 0;
    private $capital = 0;
    private $startCapital = 0;

    private $strategy = 0; // Стратегия
    private $viewDistance = 0; // За каоке расстояние актор видит событие

    private $happensEvents = [];// ID наступивших для актора события
    
    private $lucks = 0;     // Сколько раз справился
    private $unlucks = 0;   // Сколько раз облажался
    
    private $successes = 0; // Сколько раз добивался дохода
    private $faileds = 0;   // Сколько раз терпел убыток
    

    public function getSuccesses(){return $this->successes;}
    
    public function getFaileds(){return $this->faileds;}

    public function getLucks(){return $this->lucks;}
    
    public function getUnlucks(){return $this->unlucks;}

    public function getTalent(){return $this->talent;}

    public function getCapital(){return $this->capital;}

    public function getVolition(){return $this->volition;}

    /**
        Применяем событие

    */
    public function happens(array $events)
    {
        foreach($events as $event){
            // Если собитие наступило - апоминаем его
            if($this->isHappen($event)){
                if(!in_array($event->getId(), $this->happensEvents))
                    $this->happensEvents[] = $event->getId();
            }
        }
    }

    private function isHappen(Event $event)
    {
        // Событие не может произойти с персоной дважды
        if(in_array($event->getId(),$this->happensEvents))return false;

        // Расстояние от центра события до центра персоны больше суммы из радиусов
        $distance = $this->distance($event);
        if($distance > ($this->getRadius()+$event->getRadius()))
            return false;
        
        // Успех в событии, если случай [0,2] меньше таланта. 1 - средний талант 
        $luck =  $this->getTalent() > (rand()/getrandmax());
        // Приращиваем число успехов/неуспехов
        if($luck)
            $this->lucks++;
        else
            $this->unlucks++;
        
        // Если событие позитивное и удача - увеличиваем капитал на величину профита
        if($event->getProfit()>0 && $luck){
            $this->capital = $this->capital + $this->capital*$event->getProfit();
            $this->successes++;
        }elseif($event->getProfit()>0 && !$luck){
            $this->capital = $this->capital;
        }elseif($event->getProfit()<=0 && $luck){
            $this->capital = $this->capital;
        }elseif($event->getProfit()<=0 && !$luck){
            $this->capital = $this->capital-0.5*$this->capital*abs($event->getProfit());
            $this->faileds++;
        }
            
        return true;
    }

    public function setViewDistanceRange(float $M, float $Sigma)
    {
        $this->viewDistance = numeric\Random::getNormalRand($M, $Sigma);
    }

    public function setStrategyRange(int $minStrategy, int $maxStrategy)
    {
        $this->strategy = rand($minStrategy,$maxStrategy);
    }

    public function setTalentRange(float $M, float $Sigma)
    {
        $this->talent = numeric\Random::getNormalRand($M, $Sigma);
    }

    public function setCapitalRange(float $min, float $max)
    {
        $this->capital = rand($min, $max);
        $this->startCapital = $this->capital;
    }

    public function setVolitionRange(float $min, float $max)
    {
        $this->volition = rand($min, $max);

        if($this->speed>0)
           $this->speed = $this->speed*$this->volition;
        else
            $this->speed = self::MAX_SPEED*$this->volition;
    }

    /**
        Вычисляем направление движения к успешным событиям

        @param $events - массив событий
    */
    public function setVolitionDirection(array $events)
    {
        // Находим ближайшее событие
        $minDistanceSuccess = null;
        $targetEventSuccess = null;
        $minDistanceFailed = null;
        $targetEventFailed = null;

        foreach($events as $event){
            // Не рассматриваем события
            $distance = $this->distance($event);
            // Не видим событие за границей видимости
            if($distance> $this->viewDistance)continue;
            if(
                $event->getProfit()>0
                // Не стремимся за уже достигнутыми целями
                && !in_array($event->getId(),$this->happensEvents)
            ){
                if(is_null($minDistanceSuccess) || $distance<$minDistanceSuccess){
                    $minDistanceSuccess = $distance;
                    $targetEventSuccess = $event;
                }
            }
            elseif(
                $event->getProfit()<0
            ){
                if(is_null($minDistanceFailed) || $distance<$minDistanceFailed){
                    $minDistanceFailed = $distance;
                    $targetEventFailed = $event;
                }
            }
        }

        // Собственный вектор движения
        $vector = new numeric\Vector(
            $this->center->x,
            $this->center->y,
            $this->center->x+cos($this->getDirection()),
            $this->center->y+sin($this->getDirection())
        );

        // Если стратегия "Хищник"
        // Складываем собственный вектор движения с направлением на ближайшее
        // успешное событие и направлением движения события (на перехват)
        if(!is_null($targetEventSuccess) && $this->strategy&self::STRATEGY_PREDATOR){
            // Вектор на событие
            $ownVector = new numeric\Vector(
                $this->center->x,
                $this->center->y,
                $targetEventSuccess->getCenter()->x,
                $targetEventSuccess->getCenter()->y
            );

            // Вектор направления движение события
            $eventVector = new numeric\Vector(
                $targetEventSuccess->getCenter()->x,
                $targetEventSuccess->getCenter()->y,
                $targetEventSuccess->getCenter()->x
                    +cos($targetEventSuccess->getDirection()),
                $targetEventSuccess->getCenter()->y
                    +sin($targetEventSuccess->getDirection())
            );

            $vector->add($ownVector);
            $vector->add($eventVector);
        }

        // Если стратегия - жертва - ломимся от ближайшего неудачного события
        if(!is_null($targetEventFailed) && $this->strategy&self::STRATEGY_VICTIM){
            // Вектор от события
            $ownVector = new numeric\Vector(
                $this->center->x,
                $this->center->y,
                $targetEventFailed->getCenter()->x,
                $targetEventFailed->getCenter()->y
            );

            // Вектор направления движения от события
            $eventVector = new numeric\Vector(
                $targetEventFailed->getCenter()->x,
                $targetEventFailed->getCenter()->y,
                $targetEventFailed->getCenter()->x
                    +cos($targetEventFailed->getDirection()),
                $targetEventFailed->getCenter()->y
                    +sin($targetEventFailed->getDirection())
            );

            $vector->add($ownVector,-1);
        }

        
        $this->direction = $vector->getAngle();
    }


}
