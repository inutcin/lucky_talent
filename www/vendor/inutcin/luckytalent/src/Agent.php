<?php
namespace inutcin\luckytalent;

use inutcin\ageometry as ageometry;
use inutcin\ageometry\numeric as numeric;

class Agent extends ageometry\Circle
{
    
    protected $direction = null;
    protected $speed = 0;
    private $id = 0;

    function __construct(object $area, int $id=0)
    {
        $this->area = $area;
        $this->id= $id;
    }

    function getId(){
        return $this->id;
    }

    /**
        Вычисление расстояния от текущенго агента до указанного
    */
    public function distance(object $agent)
    {
        return sqrt(
            pow($this->center->x - $agent->getCenter()->x,2)
            +
            pow($this->center->y - $agent->getCenter()->y,2)
        );
    }

    public function move(){
        $dX = $this->speed*cos($this->direction);
        $dY = $this->speed*sin($this->direction);

        // Проверка выхода за за границу (отскок от стенки)
        if(
            (
                $this->getCenter()->x+$dX 
                > 
                $this->area->getCenter()->x + $this->area->getWidth()/2
            )
            ||
            (
                $this->getCenter()->x+$dX 
                < 
                $this->area->getCenter()->x - $this->area->getWidth()/2
            )
        )$dX*=-1;

        // Проверка выхода за за границу (отскок от стенки)
        if(
            (
                $this->getCenter()->y+$dY
                > 
                $this->area->getCenter()->y + $this->area->getHeight()/2
            )
            ||
            (
                $this->getCenter()->y+$dY
                < 
                $this->area->getCenter()->y - $this->area->getHeight()/2
            )
        )$dY*=-1;

        $this->center->move($dX,$dY);

        // Вычисляем новое направление
        $this->direction = numeric\Vector::getAngleByCoords($dX, $dY);
    }

    public function getSpeed(){
        return $this->speed;
    }

    public function setSpeedRange(float $min, float $max){
        $this->speed = $min+($max-$min)*(rand()/getrandmax());
    }

    function setRandomDirection(
        float $startAngle = 0, 
        float $endAngle = 2*3.1415926 // 2*Pi
    )
    {
        $this->direction = 
            $startAngle + (($endAngle-$startAngle)*(rand()/getrandmax()));
    }

    function getDirection()
    {
        return $this->direction;
    }

    function getDirectionDegree(){
        return -$this->direction*(180/pi());
    }

}
