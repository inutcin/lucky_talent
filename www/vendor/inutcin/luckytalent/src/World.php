<?php
namespace inutcin\luckytalent;

use inutcin\ageometry as ageometry;

class World extends ageometry\Area
{

    public $personsCount = 0;
    public $positiveEventsCount = 0;
    public $negativeEventsCount = 0;

    private $persons = null;
    private $events = null;
    private $scale = 1;

    private $fontSize = 8;
    private $axisFontName = "SpaceMono-Regular.ttf";
    private $img = null;
    
    private $simulationStep = 0;

    function __construct(float $width, float $height)
    {
        parent::__construct( new ageometry\Point(0,0), $width,$height);
    }


    public function create()
    {
        $eventId = 1;
        // Создание положительных событий
        for($i=0; $i<$this->positiveEventsCount;$i++){
            $event = new Event($this, $eventId);
            $event->setProfitRange(1,1);
            $event->setRadiusRange(1,1);
            $event->setRandomCenter();
            $event->setRandomDirection();
            $event->setSpeedRange(1,2);
            $this->events[] = $event;
            $eventId++;
        }
        
        // Создание отрицательных событий
        for($i=0; $i<$this->negativeEventsCount;$i++){
            $event = new Event($this, $eventId);
            $event->setProfitRange(-1,-1);
            $event->setRadiusRange(1,1);
            $event->setRandomCenter();
            $event->setRandomDirection();
            $event->setSpeedRange(1,2);
            $this->events[] = $event;
            $eventId++;
        }

        // Создание акторов
        for($i=0;$i<$this->personsCount;$i++){
            $person = new Person($this);
            $person->setRadiusRange(1,1);
            $person->setTalentRange(0.5,0.1);
            $person->setRandomCenter();
            $person->setRandomDirection();
            $person->setSpeedRange(2,2);
            $person->setVolitionRange(0,0);
            $person->setCapitalRange(1,1);
            $person->setStrategyRange(
                0
//                |Person::STRATEGY_VICTIM
                |Person::STRATEGY_PREDATOR
                ,
                0
//                |Person::STRATEGY_VICTIM
                |Person::STRATEGY_PREDATOR
            );
            $person->setViewDistanceRange(100,5);
            $this->persons[] = $person; 
        }


    }

    public function getSimulationStep()
    {
        return $this->simulationStep;
    }

    public function simulationStep(){
        
        $this->simulationStep++;
        
        // Высисляем направления движений всех акторов (стремление к успеху)
        // Двигаем всех акторов
        for($i=0;$i<count($this->persons);$i++){
            $this->persons[$i]->setVolitionDirection($this->events);
        } 

        // Двигаем всех акторов
        for($i=0;$i<count($this->persons);$i++){
            $this->persons[$i]->move();
        } 

        // Двигаем все события
        for($i=0;$i<count($this->events);$i++){
            $this->events[$i]->move();
        } 

        // Выясняем наступление всех событий для всех акторов
        for($i=0;$i<count($this->persons);$i++){
            $this->persons[$i]->happens($this->events);
        } 


    }


    /**
        Возвращает массив top самых успешных персон
    */
    public function getCapitalTop(int $top){
        $result = $this->persons;
        usort($result,function($a,$b){
            return $a->getCapital()>$b->getCapital()?-1:1;
        });
        
        if($top>0)
            return array_slice($result,0,$top);
        else
            return array_reverse(array_slice($result,count($result)+$top,-$top));
    }

    public function exportPng(float $scale=1){

        // Установка переменной окружения для GD
        putenv('GDFONTPATH=' . realpath('.'));
        
        $this->scale = $scale;

        $this->img = imagecreatetruecolor(
            $this->width*$this->scale, $this->height*$this->scale
        );

        $white = imagecolorallocate($this->img,255,255,255);
        $black = imagecolorallocate($this->img,0,0,0);

        // Рисуем фон
        imagefilledrectangle(
            $this->img,
            $this->getGDX(-$this->width/2), $this->getGDY($this->height/2),
            $this->getGDX($this->width/2), $this->getGDY(-$this->height/2),
            $white
        );

        // Рисуем события
        for($i=0;$i<count($this->events);$i++){
            $this->drawEvent($this->events[$i]);
        }

        // Рисуем людей
        for($i=0;$i<count($this->persons);$i++){
            $this->drawPerson($this->persons[$i]);
        }

        $tmpfile = tempnam(sys_get_temp_dir(),"world");
        
        $this->drawAxis($this->img);
        
        imagepng($this->img,$tmpfile);
        return file_get_contents($tmpfile);
    }

    private function drawEvent(Event $event){
        $center = $event->getCenter();
        $red  = imagecolorallocate($this->img,255,128,128);
        $green  = imagecolorallocate($this->img,128,255,128);
        $black  = imagecolorallocate($this->img,0,0,0);
        
        // Само событие
        imagefilledarc(
            $this->img,
            $this->getGDX($center->x),$this->getGDY($center->y),
            intval($event->width)*$this->scale, intval($event->height)*$this->scale+1,
            0,360,
            $event->getProfit()>0?$green:$red,
            IMG_ARC_PIE
        );

        // Направление движения
        imagefilledarc(
            $this->img,
            $this->getGDX($center->x),$this->getGDY($center->y),
            intval($event->getRadius()*2*$this->scale),
            intval($event->getRadius()*2*$this->scale),
            $event->getDirectionDegree()-5,$event->getDirectionDegree()+5,
            $black,
            IMG_ARC_PIE
        );
    }


    private function drawPerson(Person $person){
        $center = $person->getCenter();
        $blue  = imagecolorallocate($this->img,200,200,255);
        $black  = imagecolorallocate($this->img,0,0,0);
         
        imagefilledarc(
            $this->img,
            $this->getGDX($center->x),$this->getGDY($center->y),
            intval($person->width)*$this->scale, intval($person->height)*$this->scale+1,
            0,360,
            $blue,
            IMG_ARC_PIE
        );
        // Направление движения
        imagefilledarc(
            $this->img,
            $this->getGDX($center->x),$this->getGDY($center->y),
            intval($person->getVolition()*$person->getRadius()*2*$this->scale),
            intval($person->getVolition()*$person->getRadius()*2*$this->scale),
            $person->getDirectionDegree()-5,$person->getDirectionDegree()+5,
            $black,
            IMG_ARC_PIE
        );
    }


    private function drawAxis(){
        
        $axisColor = imagecolorallocate($this->img,128,128,128);
        $borderColor = imagecolorallocate($this->img,0,0,0);
        $middleColor = imagecolorallocate($this->img,220,220,220);
        
        
        // Промежуточные линии X
        for($x=-$this->width/2;$x<=$this->width/2;$x+=$this->width/8){
            imagedashedline($this->img, 
                $this->getGDX($x), $this->getGDY($this->height/2), 
                $this->getGDX($x), $this->getGDY(-$this->height/2), 
                $middleColor
            );
            
            
            // Подпись
            if($x!=0 && $x>-$this->width/2 && $x<$this->width/2)imagefttext(
                $this->img, 
                intval($this->fontSize),
                0,
                $this->getGDX($x-1.5*$this->fontSize), $this->getGDY($this->fontSize/2),
                $axisColor, __DIR__."/../resources/fonts/".$this->axisFontName,
                $x
            );
        }

        // Промежуточные линии Y
        for($y=-$this->height/2;$y<=$this->height/2;$y+=$this->height/8){
            imagedashedline($this->img, 
                $this->getGDX(-$this->width/2), $this->getGDY($y), 
                $this->getGDX($this->width/2), $this->getGDY($y), 
                $middleColor
            );

            // Подпись
            if($y!=0 && $y>-$this->height/2 && $y<$this->height/2)imagefttext(
                $this->img, 
                intval($this->fontSize),
                0,
                $this->getGDX($this->fontSize),$this->getGDY($y-0.5*$this->fontSize), 
                $axisColor, __DIR__."/../resources/fonts/".$this->axisFontName,
                $y
            );

        }


        // Ось X
        imageline($this->img, 
            $this->getGDX($this->width/2), $this->getGDY(0), 
            $this->getGDX(-$this->width/2), $this->getGDY(0), 
            $axisColor
        );

        // Ось Y
        imageline($this->img, 
            $this->getGDX(0), $this->getGDY($this->height/2), 
            $this->getGDX(0), $this->getGDY(-$this->height/2), 
            $axisColor
        );
        
        // Рамка
        imagerectangle($this->img, 
            $this->getGDX(-$this->width/2), $this->getGDY(-$this->height/2), 
            $this->getGDX($this->width/2)-1, $this->getGDY($this->height/2)-1, 
            $borderColor
        );
        
    }

    private function getGDX(float $x)
    {
        $x = $this->scale*$this->width/2 + $this->scale*$x;
        return $x;
    }

    private function getGDY(float $y)
    {
        $y = $this->scale*$this->height/2 - $this->scale*$y;
        return $y;
    }

}
